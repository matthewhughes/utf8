SRC=src/utf8.c
INC=include
TEST_SRC=t/test.c
TEST_PROG=test_run
OBJ=utf8.o
INCLUDES=-I$(INC)
WARNINGS=-pedantic -Wall -Wextra

CFLAGS=$(WARNINGS) $(INCLUDES)

.PHONY: clean

$(OBJ): $(SRC) $(INC)
	$(CC) $(CFLAGS) -o $@ -c $(SRC)

$(TEST_PROG): $(OBJ) $(TEST_SRC)
	$(CC) $(CFLAGS) $(OBJ) -o $@ $(TEST_SRC)

test: $(TEST_PROG)
	./$(TEST_PROG)

clean:
	-rm -f $(OBJ)
	-rm -f test
