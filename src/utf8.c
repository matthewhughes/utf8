#include <stdio.h>
#include "utf8.h"

static int cp_octet_count(CodePoint);
static int string_octet_count(const char *);

static const CodePoint octet_limits[UTF8_SIZE] = { 0x7f, 0x7ff, 0xffff, 0x10ffff };
static const unsigned char octet_masks[UTF8_SIZE] = {
    0x80, /* 10000000 */
    0xe0, /* 11100000 */
    0xf0, /* 11110000 */
    0xf8  /* 11111000 */
};
static const unsigned char default_mask = 0xc0; /* 11000000 */

static const unsigned char octet_bytes[UTF8_SIZE] = {
    0x00, /* 00000000 */
    0xc0, /* 11000000 */
    0xe0, /* 11100000 */
    0xf0, /* 11110000 */
};
static const unsigned char default_bytes = 0x80; /* 10000000 */

static const CodePoint surrogate_start = 0xd800;
static const CodePoint surrogate_end = 0xdfff;

static int string_octet_count(const char *c) {
    if ((unsigned char)c[0] >> 7 == 0)
        return 1;

    int count = 0;
    /* Check first byte matches one of the base bytes */
    for (int i = 1; i < UTF8_SIZE; i++)
        if ((unsigned char)c[0] >> (6 - i) == octet_bytes[i] >> (6 - i))
            count = i + 1;
    if (!count) /* Failed to match a base */
        return -1;

    /* Check remaining bytes match default */
    for (int i = 1; i < count; i++)
        if ((c[i] & default_mask) != default_bytes)
            return -1;

    return count;
}

static int cp_octet_count(CodePoint cp) {
    if (cp >= surrogate_start && cp <= surrogate_end)
        return -1;

    for (int i = 0; i < UTF8_SIZE; i++)
        if (cp <= octet_limits[i])
            return i + 1;
    return -1;
}

int encode(const char *c, CodePoint *cp) {
    int octet_count = string_octet_count(c);

    if (octet_count == -1)
        return -1;

    /* Lower bytes */
    if (octet_count > 1)
        for (int i = octet_count - 1; i > 0; i--) {
            /* mask for the current bits */
            char mask = c[i] & ~default_mask;
            *cp |= mask << (6 * (octet_count - i - 1));
        }

    /* highest byte */
    char highest_mask = c[0] & ~octet_masks[octet_count -1];
    *cp |= highest_mask << 6 * (octet_count - 1);
    return 0;
}

int decode(CodePoint cp, char *c) {
    /* 1. Determine no. bytes required */
    int clen = cp_octet_count(cp);
    if (clen == -1)
        return -1;

    /* 2. Prepare higher order bits */
    c[0] = octet_bytes[clen - 1];
    for (int i = 1; i < clen; i++)
        c[i] = default_bytes;

    /* 3. Fill bits */
    for (int i = clen - 1; i > 0; i--, cp >>= 6)
        c[i] |= cp & ~default_mask;
    c[0] |= cp & ~octet_masks[clen - 1];
    return 0;
}
