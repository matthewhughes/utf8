/*
 * https://www.unicode.org/versions/Unicode13.0.0/UnicodeStandard-13.0.pdf
 *
 */

#ifndef _MY_UTF8_H
#define _MY_UTF8_H

#include <stdint.h>
#define UTF8_SIZE 4

typedef uint_least32_t CodePoint;
int decode(CodePoint, char *);
int encode(const char *, CodePoint *);

#endif /* _MY_UTF8_H */
