# UTF8

A basic UTF8 encode and decoder. This is just a learning exercise but if you
want to play around with it you can run:

```
make test
```
