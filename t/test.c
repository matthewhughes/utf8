#include <stdio.h>
#include <string.h>
#include "utf8.h"

typedef struct BasicTest {
    CodePoint cp;
    char *decoded;
} BasicTest_t;

enum test_type {
    decoding_test,
    encoding_test,
};

typedef struct TestCase {
    const char *description;
    int type;
    CodePoint cp;
    char *decoded;
    int retval;
} TestCase_t;


int run_test(const TestCase_t *);
int run_basic_test(const BasicTest_t *);
int run_decode_test(const TestCase_t *);
int run_encode_test(const TestCase_t *);
int test_returnval(const TestCase_t *, int);
int check_decoded(const TestCase_t *, const char *);

int main()
{
    const CodePoint unicode_max = 0x10ffff;
    char invalid_utf8_byte[] = { (unsigned char)0x80 };
    char invlid_utf8_multibyte[] = {
        (unsigned char)0xe3,
        (unsigned char)0xc0
    }; /* 11100011 11000000 */

    const BasicTest_t basic_tests[] = {
        {0xe9, "é"},
        {0x61, "a"},
        {0x30f0, "ヰ"},
        {0x10102, "𐄂"},
        {0x1f370, "🍰"}
    };
    const TestCase_t tests[] = {
        {
            "Testing larger than allowed code point",
            decoding_test,
            unicode_max + 1,
            NULL,
            -1,
        },
        {
           "Testing surrogate code point",
           decoding_test,
           0xd811,
           NULL,
           -1,
        },
        {
            "Testing invalid utf-8 byte",
            encoding_test,
            0xe9,
            invalid_utf8_byte,
            -1,
        },
        {
            "Testing invalid utf-8 multibyte",
            encoding_test,
            0,
            invlid_utf8_multibyte,
            -1,
        },
    };
    const size_t basic_test_count = sizeof(basic_tests)/sizeof(BasicTest_t);
    const size_t test_count = sizeof(tests)/sizeof(TestCase_t);

    int success = 0;
    for (size_t i = 0; i < basic_test_count; i++)
        if (run_basic_test(&basic_tests[i]))
            success = 1;

    for (size_t i = 0; i < test_count; i++)
        if (run_test(&tests[i]))
            success = 1;

    return success;
}

int run_test(const TestCase_t *test) {
    if (test->description != NULL)
        printf("%s\n", test->description);

    switch (test->type) {
        case decoding_test:
            return run_decode_test(test);
        case encoding_test:
            return run_encode_test(test);
    }
    return 1;
}

int run_decode_test(const TestCase_t *test) {
    char decoded[UTF8_SIZE] = { '\0' };

    int retval = decode(test->cp, decoded);

    /* Don't check decode string if something went wrong */
    if (test->retval != 0 || retval != 0)
        return test_returnval(test, retval);

    if (check_decoded(test, decoded) != 0) {
        fprintf(stderr, "Failed decoding 0x%x: got %s, expected %s",
            test->cp, decoded, test->decoded);
        return 1;
    }
    return 0;
}

int run_encode_test(const TestCase_t *test) {
    CodePoint cp = 0;

    int retval = encode(test->decoded, &cp);
    if (test->retval != 0 || retval != 0)
        return test_returnval(test, retval);

    if (cp != test->cp) {
        fprintf(stderr, "Failed encoding '%s': got 0x%x, expected 0x%x\n",
            test->decoded, cp, test->cp);
        return 1;
    }
    return 0;
}

int check_decoded(const TestCase_t *test, const char *expected) {
    char *decoded = test->decoded;
    for (int i = 0; i < UTF8_SIZE && decoded[i]; i++)
        if (decoded[i] != expected[i])
            return 1;
    return 0;
}

int test_returnval(const TestCase_t *test, int retval) {
    if (retval != test->retval) {
        fprintf(stderr, "Failed return value check, got: %d, expected: %d\n",
            retval,
            test->retval
        );
        return 1;
    }
    return 0;
}

int run_basic_test(const BasicTest_t *test) {
    int success = 0;
    char decode_description[40];
    sprintf(decode_description, "Testing %s <- 0x%x ", test->decoded, test->cp);
    TestCase_t decode_test = {
        decode_description,
        decoding_test,
        test->cp,
        test->decoded,
        0,
    };

    char encode_description[40];
    sprintf(encode_description, "Testing %s -> 0x%x", test->decoded, test->cp);
    TestCase_t encode_test = {
        encode_description,
        encoding_test,
        test->cp,
        test->decoded,
        0,
    };

    if (run_test(&decode_test))
        success = 1;
    if (run_test(&encode_test))
        success = 1;

    return success;
}
